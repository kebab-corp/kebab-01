(params) => {
  let name = params.name
  let cmds =  {
    bob: ["salade", "tomates", "oignons", "veau", "harissa"],
    sam: ["salade", "tomates", "veau"],
    john: ["salade", "veau"],
    paul: ["salade", "tomate", "oignons", "poulet", "curry", "frites"],
    babs: ["assiette-kebab", "salade", "tomates", "veau", "riz"],
    bill: ["👋 hello world 🌍"]
  }
  return cmds[name]
}
